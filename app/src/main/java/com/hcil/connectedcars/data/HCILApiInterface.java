package com.hcil.connectedcars.data;

import com.hcil.connectedcars.data.request_pojo.VerifyContactNumPojo;
import com.hcil.connectedcars.data.request_pojo.VerifyOTPPojo;
import com.hcil.connectedcars.data.verifycontactnumber.NewVerifyContactNumPojo;
import com.hcil.connectedcars.data.verifysiginotp.VerifyOTPContributor;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface HCILApiInterface {


    @POST("/bos/customer/verifyPrimaryContactNo")
    @Headers({ "Content-Type: application/json"})
    Call<NewVerifyContactNumPojo> verifyPrimaryContactNo(@Body VerifyContactNumPojo verifyContactNumPojo);


    @POST("/bos/authentication/verifyOtpPin")
    @Headers({ "Content-Type: application/json"})
    Call<VerifyOTPContributor> verifyOtpPin(@Body VerifyOTPPojo verifyOTPPojo);


    @POST("/bos/customer/createMpin")
    @Headers({"Content-Type: application/json"})
    Call<VerifyOTPContributor> createMpin(@Header("mpin") String mpin, @Body VerifyContactNumPojo verifyContactNumPojo);



}
