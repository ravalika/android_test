package com.hcil.connectedcars.data.request_pojo;

public class VerifyOTPPojo {
    String key;
    String otp;
    String primaryMobileNo;
    String emailId;

    public VerifyOTPPojo(String key, String otp, String primaryMobileNo, String emailId) {
        this.key = key;
        this.otp = otp;
        this.primaryMobileNo = primaryMobileNo;
        this.emailId = emailId;
    }
}
