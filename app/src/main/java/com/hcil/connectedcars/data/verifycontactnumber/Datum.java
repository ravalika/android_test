
package com.hcil.connectedcars.data.verifycontactnumber;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("key")
    @Expose
    private String key;
    @SerializedName("generatedOtp")
    @Expose
    private String generatedOtp;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getGeneratedOtp() {
        return generatedOtp;
    }

    public void setGeneratedOtp(String generatedOtp) {
        this.generatedOtp = generatedOtp;
    }

}
