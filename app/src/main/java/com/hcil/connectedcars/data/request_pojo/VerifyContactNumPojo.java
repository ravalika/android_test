package com.hcil.connectedcars.data.request_pojo;

public class VerifyContactNumPojo {
    public String primaryMobileNo;
    public String emailId;


    public VerifyContactNumPojo(String primaryMobileNo, String emailId) {
        this.primaryMobileNo = primaryMobileNo;
        this.emailId = emailId;
    }

    public String getPrimaryMobileNo() {
        return primaryMobileNo;
    }

    public void setPrimaryMobileNo(String primaryMobileNo) {
        this.primaryMobileNo = primaryMobileNo;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
