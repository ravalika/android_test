
package com.hcil.connectedcars.data.mpin;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MPinContributor {

    @SerializedName("StatusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;
    @SerializedName("vehicleDetails")
    @Expose
    private List<VehicleDetail> vehicleDetails = null;
    @SerializedName("secondaryOwner")
    @Expose
    private List<SecondaryOwner> secondaryOwner = null;

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public List<VehicleDetail> getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(List<VehicleDetail> vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public List<SecondaryOwner> getSecondaryOwner() {
        return secondaryOwner;
    }

    public void setSecondaryOwner(List<SecondaryOwner> secondaryOwner) {
        this.secondaryOwner = secondaryOwner;
    }

}
