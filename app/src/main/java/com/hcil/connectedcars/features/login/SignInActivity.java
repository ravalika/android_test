package com.hcil.connectedcars.features.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.hcil.connectedcars.HCILApplicatioin;
import com.hcil.connectedcars.R;

import com.hcil.connectedcars.data.HCILApiInterface;
import com.hcil.connectedcars.data.request_pojo.VerifyContactNumPojo;
import com.hcil.connectedcars.data.request_pojo.VerifyOTPPojo;
import com.hcil.connectedcars.data.verifycontactnumber.NewVerifyContactNumPojo;
import com.hcil.connectedcars.data.verifysiginotp.Datum;
import com.hcil.connectedcars.data.verifysiginotp.VerifyOTPContributor;
import com.hcil.connectedcars.features.mpin.MPinActivity;

import java.util.List;
import java.util.regex.Pattern;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignInActivity extends AppCompatActivity {
    EditText mobileEditText;

    @Inject
    SharedPreferences appSharedPref;
    @Inject
    HCILApiInterface apiInterface;
    private ImageButton img_btn_sign;
    private EditText edt_sign_input;
    String emailMobilePattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+\\.+[a-z]+";
    private LinearLayout layout_error;
    private ViewSwitcher simpleViewSwitcher;
    private LinearLayout layout_edt_sign;
    private LinearLayout layout_otp;
    private ViewSwitcher vsw_sign;
    private ProgressBar progress_otp;
    private Handler handler = new Handler();
    int progressStatus=0;
    int pStatus = 100;
    private TextView txt_prg_counter;
    private EditText edt_otp;
    private String KEY;
    private TextView txt_change_mob;
    private TextView txt_sigin_input_error;
    private TextView txt_entered_user_mob;
    private LinearLayout layout_otp_resend;
    private CountDownTimer otp_time_counter;
    private TextView txt_otp_resend;
    private TextView txt_otp_error;
    private LinearLayout layout_otp_error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        ((HCILApplicatioin) getApplication()).getComponents().inject(this);
        setContentView(R.layout.activity_new_signin);
        init();

        progress_otp.setProgress(0);   // Main Progress
        img_btn_sign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             if(!edt_sign_input.getText().toString().trim().matches(emailMobilePattern)){
                 verifyPrimaryMobileNo();

             }
             else
             {
                 Toast.makeText(getApplicationContext(),"Please enter mobile no only",Toast.LENGTH_SHORT).show();
             }

            }
        });
        simpleViewSwitcher=(ViewSwitcher)findViewById(R.id. vsw_sign); // initiate a ViewSwitcher
        Animation in = AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left); // load an animation
        simpleViewSwitcher.setInAnimation(in);

        final String email = edt_sign_input.getText().toString().trim();
        edt_sign_input.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

                if ((edt_sign_input.getText().toString().trim().matches(emailMobilePattern) && s.length() > 0) || (isValidMobile(edt_sign_input.getText().toString())))
                {
                // Toast.makeText(getApplicationContext(),"valid email address",Toast.LENGTH_SHORT).show();
                   showSignINError(false,getString(R.string.msg_sign_in_error));
                    img_btn_sign.setImageResource(R.drawable.ic_right_icon_arrow_red);
                    Log.e("edt_input","valid");
                }
                else
                {
                 //Toast.makeText(getApplicationContext(),"Invalid email address",Toast.LENGTH_SHORT).show();
                    //layout_error.setVisibility(View.VISIBLE);
                    showSignINError(true,getString(R.string.msg_sign_in_error));
                    img_btn_sign.setImageResource(R.drawable.ic_right_icon_arrow);

                    Log.e("edt_input","in valid");

                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // other stuffs
            }
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // other stuffs
            }
        });
        edt_otp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edt_otp.getText().length()==4){
                    callVerifyOtp();
                }

            }
        });

        txt_change_mob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopOTPProgress();
                showSignInLayout();

            }
        });

        txt_otp_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyPrimaryMobileNo();
                showOTPProgress();

            }
        });

    }

    private void showSignINError(boolean showErrorFlag, String msg) {
        if(showErrorFlag){
            layout_error.setVisibility(View.VISIBLE);
        }else{
            layout_error.setVisibility(View.GONE);
        }
        txt_sigin_input_error.setText(msg);
    }

    private void showOTPError(boolean showErrorFlag, String msg) {
        if(showErrorFlag){
            layout_otp_error.setVisibility(View.VISIBLE);
        }else{
            layout_otp_error.setVisibility(View.GONE);
        }
        txt_otp_error.setText(msg);
    }

    private void init() {
        img_btn_sign=(ImageButton)findViewById(R.id.img_btn_sign);
        edt_sign_input=(EditText)findViewById(R.id.edt_sign_input);
        layout_error=(LinearLayout)findViewById(R.id.layout_error);
        vsw_sign=(ViewSwitcher)findViewById(R.id.vsw_sign);
        layout_edt_sign=(LinearLayout)findViewById(R.id.layout_edt_sign);
        layout_otp=(LinearLayout)findViewById(R.id.layout_otp);
        layout_otp_resend=(LinearLayout)findViewById(R.id.layout_otp_resend);
        layout_otp_error=(LinearLayout)findViewById(R.id.layout_otp_error);
        progress_otp = (ProgressBar) findViewById(R.id.progress_otp);
        txt_prg_counter=(TextView)findViewById(R.id.txt_prg_counter);
        edt_otp=(EditText)findViewById(R.id.edt_otp);
        txt_change_mob=(TextView)findViewById(R.id.txt_change_mob);
        txt_otp_resend=(TextView)findViewById(R.id.txt_otp_resend);
        txt_sigin_input_error=(TextView)findViewById(R.id.txt_sigin_input_error);
        txt_entered_user_mob=(TextView)findViewById(R.id.txt_entered_user_mob);
        txt_otp_error=(TextView)findViewById(R.id.txt_otp_error);

    }

    private void callVerifyOtp() {

        VerifyOTPPojo verifyOTPPojo=new VerifyOTPPojo(KEY,edt_otp.getText().toString(),edt_sign_input.getText().toString(),"");
        apiInterface.verifyOtpPin(verifyOTPPojo).enqueue(new Callback<VerifyOTPContributor>() {
            @Override
            public void onResponse(Call<VerifyOTPContributor> call, Response<VerifyOTPContributor> response) {
                List<Datum> data = response.body().getData();
                if(data!=null)
                {
                    if(data.get(0).getOtpStatus().equalsIgnoreCase("True")){
                        Intent intent=new Intent(SignInActivity.this, MPinActivity.class);
                        startActivity(intent);
                        finish();

                    }else{
                    edt_otp.setText("");
                        showOTPError(true,response.body().getResponse());
                    }
                    Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<VerifyOTPContributor> call, Throwable t) {

            }
        });
    }

    private void verifyPrimaryMobileNo() {
        VerifyContactNumPojo verifyContactNumPojo=new VerifyContactNumPojo(edt_sign_input.getText().toString(),"");
        apiInterface.verifyPrimaryContactNo(verifyContactNumPojo).enqueue(new Callback<NewVerifyContactNumPojo>() {
            @Override
            public void onResponse(Call<NewVerifyContactNumPojo> call, Response<NewVerifyContactNumPojo> response) {
                if(response.body().getData()!=null){
                    KEY=response.body().getData().get(0).getKey();
                    Log.e("Key",response.body().getData().get(0).getKey());
                    Log.e("OTP",response.body().getData().get(0).getGeneratedOtp());
                    txt_entered_user_mob.setText("+91 "+edt_sign_input.getText().toString());
                    showOTPLayout();
                    Toast.makeText(getApplicationContext(),response.body().getData().get(0).getGeneratedOtp(),Toast.LENGTH_SHORT).show();


                }
                else{
                    showSignINError(true,response.body().getErrorMsg());
                }

            }

            @Override
            public void onFailure(Call<NewVerifyContactNumPojo> call, Throwable t) {

            }
        });
    }

    public void showOTPLayout() {
        if (vsw_sign.getCurrentView() != layout_otp) {
            vsw_sign.showNext();
            showOTPProgress();
            progress_otp.setProgress(0);


        }
    }

    public void showSignInLayout(){

        if (vsw_sign.getCurrentView() != layout_edt_sign) {
            vsw_sign.showPrevious();
            layout_otp_resend.setVisibility(View.GONE);
        }

    }

    private boolean isValidMobile(String phone) {

        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            //if(phone.length() < 6 && phone.length() > 11) {
                if(phone.length() != 10) {
                check = false;

            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }


    public void showOTPProgress(){
        layout_otp_resend.setVisibility(View.GONE);
        pStatus=100;
        progress_otp.setProgress(100);
        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus >=0) {
                    pStatus -= 1;

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            progress_otp.setProgress(pStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        // Just to display the progress slowly
                        Thread.sleep(300); //thread will take approx 1 seconds to finish
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        otp_time_counter=  new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                txt_prg_counter.setText(""+ millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                //mTextField.setText("done!");
                layout_otp_resend.setVisibility(View.VISIBLE);
            }

        }.start();

    }

    public void stopOTPProgress(){
        otp_time_counter.cancel();
        handler.removeCallbacksAndMessages(null);
    }

}


/** important  links
*
 * https://stackoverflow.com/questions/39445898/javax-net-ssl-sslpeerunverifiedexception-error-in-retrieving-session-key-from-sk
* */