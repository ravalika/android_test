package com.hcil.connectedcars.features.mpin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.hcil.connectedcars.HCILApplicatioin;
import com.hcil.connectedcars.R;
import com.hcil.connectedcars.data.HCILApiInterface;
import com.hcil.connectedcars.data.request_pojo.VerifyContactNumPojo;
import com.hcil.connectedcars.data.verifycontactnumber.NewVerifyContactNumPojo;
import com.hcil.connectedcars.data.verifysiginotp.VerifyOTPContributor;
import com.hcil.connectedcars.features.home.HomeActivity;
import com.hcil.connectedcars.view.PinEntryEditText;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MPinActivity extends AppCompatActivity {

    private Button btn_submit_mpin;
    private PinEntryEditText edt_pin1;
    private PinEntryEditText edt_pin_confirm;
    @Inject
    HCILApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((HCILApplicatioin) getApplication()).getComponents().inject(this);
        setContentView(R.layout.activity_mpin);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        edt_pin1=(PinEntryEditText)findViewById(R.id.edt_pin1);
        edt_pin_confirm=(PinEntryEditText)findViewById(R.id.edt_pin_confirm);
        btn_submit_mpin=(Button)findViewById(R.id.btn_submit_mpin);

        btn_submit_mpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(edt_pin1.getText().toString().equals(edt_pin_confirm.getText().toString()))
                {
                    VerifyContactNumPojo verifyContactNumPojo=new VerifyContactNumPojo("9029164676","");
                    apiInterface.createMpin(edt_pin_confirm.getText().toString(),verifyContactNumPojo).enqueue(new Callback<VerifyOTPContributor>() {
                        @Override
                        public void onResponse(Call<VerifyOTPContributor> call, Response<VerifyOTPContributor> response) {
                            Toast.makeText(getApplicationContext(),response.body().getMessage(),Toast.LENGTH_SHORT).show();
                            Intent i=new Intent(MPinActivity.this, HomeActivity.class);
                            startActivity(i);
                            finish();
                        }

                        @Override
                        public void onFailure(Call<VerifyOTPContributor> call, Throwable t) {

                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(),"Please enter same pin correctly",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}
