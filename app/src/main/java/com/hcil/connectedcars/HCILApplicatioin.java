package com.hcil.connectedcars;

import android.app.Application;

import com.hcil.connectedcars.di.component.AppComponents;

import com.hcil.connectedcars.di.component.DaggerAppComponents;
import com.hcil.connectedcars.di.module.AppModule;
import com.hcil.connectedcars.di.module.RetrofitModule;

public class HCILApplicatioin extends Application {

    private AppComponents component;

    @Override
    public void onCreate() {
        super.onCreate();

        component= DaggerAppComponents.builder()
                .appModule(new AppModule(this))
                .retrofitModule(new RetrofitModule(this))
                .build();
    }
    public AppComponents getComponents() {
        return component;
    }
}
