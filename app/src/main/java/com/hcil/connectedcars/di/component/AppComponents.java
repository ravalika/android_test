package com.hcil.connectedcars.di.component;

import com.hcil.connectedcars.MainActivity;
import com.hcil.connectedcars.di.module.AppModule;
import com.hcil.connectedcars.di.module.RetrofitModule;
import com.hcil.connectedcars.features.login.SignInActivity;
import com.hcil.connectedcars.features.mpin.MPinActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, RetrofitModule.class})
public interface AppComponents {

    void inject(MainActivity activity);
    void inject(RetrofitModule module);
    void inject(SignInActivity activity);
    void inject(MPinActivity activity);

}
