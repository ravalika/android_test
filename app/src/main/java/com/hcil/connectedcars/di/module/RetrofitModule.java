package com.hcil.connectedcars.di.module;

import android.content.Context;
import android.util.Log;

import com.hcil.connectedcars.HCILApplicatioin;
import com.hcil.connectedcars.R;
import com.hcil.connectedcars.data.HCILApiInterface;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class RetrofitModule {


    @Inject
    Context context;

    public RetrofitModule(HCILApplicatioin applicatioin) {
        context=applicatioin;
    }

    @Provides
    @Singleton
    HCILApiInterface getApiInterface(Retrofit retroFit) {
        return retroFit.create(HCILApiInterface.class);
    }

    @Provides
    @Singleton
    Retrofit getRetrofit(OkHttpClient okHttpClient) {

        if(context!=null)
        {
            Log.e("Context","not null");
        }
        else
        {
            Log.e("Context","null");
        }


        return new Retrofit.Builder()
                .baseUrl("https://169.38.98.215:7143/")
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(getUnsafeOkHttpClient().build())
               //.client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    OkHttpClient getOkHttpCleint(HttpLoggingInterceptor httpLoggingInterceptor) {

        try {

            // Load CAs from an InputStream
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");

            InputStream inputStream = context.getResources().openRawResource(R.raw.bos_dev); //(.crt)
            Certificate certificate = certificateFactory.generateCertificate(inputStream);
            inputStream.close();

            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", certificate);

            // Create a TrustManager that trusts the CAs in our KeyStore.
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm);
            trustManagerFactory.init(keyStore);

            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            X509TrustManager x509TrustManager = (X509TrustManager) trustManagers[0];


            // Create an SSLSocketFactory that uses our TrustManager
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[]{x509TrustManager}, null);
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();


            return new OkHttpClient.Builder()
                    .addInterceptor(httpLoggingInterceptor)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .sslSocketFactory(getSSLConfig(context).getSocketFactory(),x509TrustManager)
                    .hostnameVerifier(new HostnameVerifier() {
                        @Override
                        public boolean verify(String s, SSLSession sslSession) {
                            HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
                            return hv.verify("localhost", sslSession);
                        }
                    })


                    .build();
        } catch (Exception ex){
            Log.e("OkkHttp","inside exception");
            ex.printStackTrace();
            return new OkHttpClient();
        }
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return httpLoggingInterceptor;
    }

    @Provides
    @Singleton
    public   SSLContext getSSLConfig(Context context) {

        SSLContext sslContext = null;

        // Loading CAs from an InputStream
       try {
           CertificateFactory cf = null;
           cf = CertificateFactory.getInstance("X.509");

           Certificate ca;
           // I'm using Java7. If you used Java6 close it manually with finally.
           try (InputStream cert = context.getResources().openRawResource(R.raw.bos_dev)) {
               ca = cf.generateCertificate(cert);
           }

           // Creating a KeyStore containing our trusted CAs
           String keyStoreType = KeyStore.getDefaultType();
           KeyStore keyStore = KeyStore.getInstance(keyStoreType);
           keyStore.load(null, null);
           keyStore.setCertificateEntry("ca", ca);

           // Creating a TrustManager that trusts the CAs in our KeyStore.
           String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
           TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
           tmf.init(keyStore);

           // Creating an SSLSocketFactory that uses our TrustManager
            sslContext = SSLContext.getInstance("TLS");
           sslContext.init(null, tmf.getTrustManagers(), null);
           return sslContext;

       }catch (Exception ex)
       {
           Log.e("getSSLConfig","inside exception");
           return sslContext;


       }


    }

    @Provides
    @Singleton
    public  OkHttpClient.Builder getUnsafeOkHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.addInterceptor(getHttpLoggingInterceptor());
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            return builder;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}


/** Imp links
 *https://adiyatmubarak.wordpress.com/tag/add-ssl-certificate-in-retrofit-2/
 *http://qaru.site/questions/2277067/javaxnetsslsslpeerunverifiedexception-error-in-retrieving-session-key-from-skyscanner-api-android
 *https://marthinmhpakpahan.wordpress.com/2016/10/26/implement-ssl-android-retrofit/
 *
 * vvimp
 * https://stackoverflow.com/questions/33539929/how-to-solve-javax-net-ssl-sslpeerunverifiedexception-hostname-com-not-verifie
 * * */
